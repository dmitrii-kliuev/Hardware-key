# Hardware-key
Hardware key based on Atmega32u4 microcontroller for software protection. The goal of the project: to protect critical software data and user licensing.

# Explanatory note
https://github.com/dmitrii-kliuev-1989/Hardware-key/blob/master/!Documentation/PDF/Explanatory_note-K06-12B-BKP-Kliuev.pdf
